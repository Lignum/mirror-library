﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Levelname : MonoBehaviour {
		
	Text text;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();

		string name = "";
		int level = GlobalVariables.levelNum;

		if(level<99) name+="0";
		if(level<9) name+="0";

		name += level+1;

		text.text = "LEVEL " + name;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
