﻿using UnityEngine;
using System.Collections;

using GoogleMobileAds.Api;

public class AdsMng : MonoBehaviour {

	BannerView bannerView;

	// Use this for initialization
	void Start () {
		RequestBanner();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void showBanner(bool show){
		if(show) bannerView.Show();
		else bannerView.Hide();
	}

	public void RequestBanner(){
		
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-1359355971647353/5294491625";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
		
	}
}
