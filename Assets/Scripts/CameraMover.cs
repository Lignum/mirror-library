﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {

	public bool active;

	private Camera _camera;

	private Vector2 _mousePos;
	private Vector3 _initPos;
	private Vector2 _maxPos;
	private Vector2 _maxBorder;

	public GameObject options;

	// Use this for initialization
	void Start () {
		active = false;
		_camera = Camera.main;
		_initPos = _camera.transform.position;

		_maxBorder = new Vector2(8.8f, 7);
		//Debug.Log(_initPos);
		//_initPos = new Vector3(12.2f,7,-10);
	}
	
	// Update is called once per frame
	void Update () {

		if(active){

			move();
		}
		//_camera.transform.Translate(new Vector3(0.1f,0,0));
	}

	public void setMaxPos(float x, float y){
		_maxPos = new Vector2(x, y);
	}

	public void move(){

		bool move = false;
		
		if(!options) move = true;
		else if(!options.activeInHierarchy) move = true;
		//		Debug.Log(_initPos);
		//Debug.Log(move);

		if(move){

			if(Input.GetMouseButtonDown(0)){
				_mousePos = Camera.main.ScreenPointToRay (Input.mousePosition).origin;
			}
			
			if(Input.GetMouseButton(0)){
				Vector2 pos = Camera.main.ScreenPointToRay (Input.mousePosition).origin;
				
				Vector2 dir = _mousePos -pos;
				
				_camera.transform.Translate(dir);
				
				reajustPos();
				
			}
		}
	}

	public void changeSize(int size){

		Camera.main.orthographicSize += size;

		if(Camera.main.orthographicSize < 7){
			Camera.main.orthographicSize = 7;
		}else{

			if(testSize(size)){
			
				_initPos.x += size*1.8f;
				_initPos.y += size*1f;

				_maxBorder.x += size*1.25f;
				_maxBorder.y += size*1f;
				
				float scale = Camera.main.orthographicSize/7;
				
				Camera.main.transform.localScale = new Vector3(scale, scale, scale);
			}else{
				
				Camera.main.orthographicSize -= size;
			}
			
		}

		reajustPos();
	}

	private void reajustPos(){
		if(_camera.transform.position.x < _initPos.x){
			_camera.transform.position = new Vector3(_initPos.x, _camera.transform.position.y, _initPos.z);
		}
		if(_camera.transform.position.y < _initPos.y){
			_camera.transform.position = new Vector3(_camera.transform.position.x, _initPos.y, _initPos.z);
		}
		
		if(_camera.transform.position.x > _maxPos.x-_maxBorder.x){
			_camera.transform.position = new Vector3(_maxPos.x-_maxBorder.x, _camera.transform.position.y, _initPos.z);
		}
		if(_camera.transform.position.y > _maxPos.y-_maxBorder.y){
			_camera.transform.position = new Vector3(_camera.transform.position.x, _maxPos.y-_maxBorder.y, _initPos.z);
		}
	}

	private bool testSize(int size){

		bool hor = true;

		if(_initPos.x + size*1.8f > (_maxPos.x-_maxBorder.x - size*1.25f)+1){
			hor = false;
		}

		bool ver = true;
		
		if(_initPos.y + size*1f > (_maxPos.y-_maxBorder.y - size*1f)+1){
			ver = false;
			
			//Debug.Log("----");
			//Debug.Log((_initPos.y + size*1f) - (_maxPos.y-_maxBorder.y - size*1f));
			//Debug.Log(_maxPos.y-_maxBorder.y - size*1f);
		}


		return hor && ver;
	}
}
