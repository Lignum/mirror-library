﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundMng : MonoBehaviour {

	private Slider slider;
	// Use this for initialization
	void Start () {
		slider = gameObject.GetComponent<Slider>();

		slider.value = AudioListener.volume;
	}
	
	// Update is called once per frame
	void Update () {
		AudioListener.volume = slider.value;
	}
}
