﻿using UnityEngine;
using System.Collections;

public class WallScr : MonoBehaviour {


	public int key;
	private WallMng _wallMng;

	public bool up, down, left, right, rampUpLeft, rampUpRight, rampDownLeft, rampDonwRight;

	public LayerMask placedObjects;

	private static Vector2 POS_UP = new Vector2(0,1);
	private static Vector2 POS_DOWN = new Vector2(0,-1);
	private static Vector2 POS_LEFT = new Vector2(-1,0);
	private static Vector2 POS_RIGHT = new Vector2(1,0);
	
	private static Vector2 POS_UP_LEFT = new Vector2(-1f,1f);
	private static Vector2 POS_UP_RIGHT = new Vector2(1f,1f);
	private static Vector2 POS_DOWN_LEFT = new Vector2(-1f,-1f);
	private static Vector2 POS_DOWN_RIGHT = new Vector2(1f,-1f);

	private Vector2[] positions = new Vector2[4];

	//private LayerMask layerMask;

	private float _mirrorDes = 0.9f;
	private float _mirrorRampDes = 0.45f;

	void Awake(){
		
		_wallMng = GameObject.Find("_WallMng").GetComponent<WallMng>();
	}

	// Use this for initialization
	void Start () {


		//layerMask = 1 << LayerMask.NameToLayer ("Mirror");


	}
	
	// Update is called once per frame
	void Update () {

	}

	private void checkPlacePositions(){
		int p = 0;
		
		if(up){
			positions[p] = POS_UP;
			p++;
		}
		if(right){
			positions[p] = POS_RIGHT;
			p++;
		}
		if(down){
			positions[p] = POS_DOWN;
			p++;
		}
		if(left){
			positions[p] = POS_LEFT;
			p++;
		}
		
		if(rampUpLeft){
			positions[p] = POS_UP_LEFT.normalized;
			p++;
		}
		if(rampUpRight){
			positions[p] = POS_UP_RIGHT.normalized;
			p++;
		}
		if(rampDownLeft){
			positions[p] = POS_DOWN_LEFT.normalized;
			p++;
		}
		if(rampDonwRight){
			positions[p] = POS_DOWN_RIGHT.normalized;
			p++;
		}

	}

	public bool canPlaceObject(Vector2 normal){

		//Debug.Log("Can place normal: " + normal);


		bool place = false;

		checkPlacePositions();

		for(int i=0; i<positions.Length; i++){
			
			//Debug.Log("Can place normal2: " + positions[i]);
			if(normal == positions[i]){
				place = true;
				break;
			}
		}

		if(place){
			Vector2 pos = transform.position;
			pos += normal;

			RaycastHit2D hit = Physics2D.Raycast(pos, normal, 0.5f, placedObjects);
			//Debug.Log(transform);
			if(hit)	place = false;

			/*RaycastHit2D[]  hits = Physics2D.RaycastAll(transform.position, normal, 1, placedObjects);
			foreach(RaycastHit2D t in hits){
				if( !t.transform.IsChildOf(transform)) place = false;
			}*/
		}

		
		//Debug.DrawLine(transform.position, transform.position + new Vector3( normal.x, normal.y, 0));
		//Debug.Log("Can place: " + place);

		return place;
	}

	public GameObject placeObject(GameObject obj, Vector2 normal, bool aplyDes = true){
							
		//Debug.Log(normal);

		Vector3 pos = normal;

		if(aplyDes){

			if((normal.x != 1 && normal.x != 0 && normal.x != -1) && transform.up == Vector3.up){			
				pos *= _mirrorRampDes;
			}else{
				pos *= _mirrorDes;
			}
		}

		GameObject mir = (GameObject) Instantiate(obj, transform.position + pos, Quaternion.identity);

		mir.transform.up = normal;

		if(mir.transform.rotation.x == 1){
			mir.transform.rotation = new Quaternion(0,0,1,0);
		}

		mir.transform.parent = this.transform;

		return mir;
	}

	public void setKey(string key){
		//Debug.Log(key);
		_wallMng.changeButtonKey(this, key);
		//Debug.Log(this.key);
	}

	public virtual void setKey(int key){
		_wallMng.changeButtonKey(this, key);		
	}

	public string getKey(){
		return _wallMng.keys[key];
	}

}
