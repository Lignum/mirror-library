﻿using UnityEngine;
using System.Collections;

using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveData{

	public static bool[] data = new bool[100];
	public static int block = 0;



	public static void levelComplete(int level){
		data[level] = true;
		calculateBlock();
		saveData();
	}

	private static void saveData(){
		
		Debug.Log("saving");

		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (FilePath());
		bf.Serialize(file, data);
		file.Close();

		Debug.Log("save");
	}

	public static void loadData(){
		if(File.Exists(FilePath())){
			
			Debug.Log("loading");

			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(FilePath(), FileMode.Open);
			bool[] loaded = (bool[]) bf.Deserialize(file);
			//myInt = loaded.myInt;
			data = loaded;
			file.Close();
		}

		Debug.Log("load");
		calculateBlock();

		//showData();
	}

	private static void calculateBlock(){
		for(int i=0; i<data.Length; i++){
			if(data[i] == true) block = i+1;
		}


		//DEBUG
		//block = 100;
	}

	private static void showData(){

		for(int i=0; i<data.Length; i++){
			Debug.Log(i + ": " + data[i]);
		}

	}

	public static void eraseData(){
		for(int i=0; i<data.Length; i++){
			data[i] = false;
		}

		block = 0;
		saveData();
	}

	private static string FilePath(){
		return Application.persistentDataPath + "/data.sav";
	}
}
