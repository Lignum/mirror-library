using UnityEngine;
using System.Collections;

using UnityEngine.EventSystems;

public class MirrorMng : Manager {

	public CheckButtons buttons;

	public GameObject[] mirrors;
	public int[] quantity;
	
	public LayerMask layerMask;

	private Vector2 origin;
	private RaycastHit2D[] hit;

	private bool place;
	private float placeCountDown;

	private AudioSource sound;
	private AudioSource eraseSound;

	public GameObject options;

	// Use this for initialization
	void Start () {
		place = true;
		hit = new RaycastHit2D[8];

		sound = gameObject.GetComponent<AudioSource>();
		eraseSound = gameObject.GetComponents<AudioSource>()[1];
	}
	
	// Update is called once per frame
	void Update () {

		if(!options.activeInHierarchy){

			if(placeCountDown>0){
				placeCountDown -= Time.deltaTime;
			}

			if(!EventSystem.current.IsPointerOverGameObject()){
				if(Input.GetMouseButtonDown(0)){
					placeCountDown = 0.25f;
				}
				if(Input.GetMouseButtonUp(0)){

					if(placeCountDown>0) place = true;

					if(place){
						if(_type == -1){
							removeMirror();
						}else{
							if(!rotateMirror()){
								if(quantity[_type] != 0){
									placeMirror();
								}
							}
						}
					}else{
						place = true;
					}
				}else if(Input.GetMouseButton(0)){
					place = false;
					GetComponent<CameraMover>().move();
				}
			}
		}

	}

	private void placeMirror(){

		//Debug.Log("place");

		origin = Camera.main.ScreenPointToRay(Input.mousePosition).origin;
		
		int i=-1;
		int j=-1;
		
		int count = 0;
		
		while(j<=1){
			if(i==0 && j==0) i++;
			
			hit[count] = Physics2D.Raycast(origin, new Vector3(i,j,0), 1, layerMask);
			
			count++;
			
			if(i>=1){
				i=-1;
				j++;
			}else{
				i++;
			}
			
			
		}
		
		float minDistance = 100f;
		int pos = 0;
		WallScr wall = null;
		WallScr placeWall = null;


		for(int e=0; e<hit.Length; e++){
			if(hit[e].collider){

				if(hit[e].distance < minDistance){
					wall = hit[e].collider.gameObject.GetComponent<WallScr>();

					if(!wall){						
						wall = hit[e].transform.parent.GetComponent<WallScr>();
					}

					//Debug.Log(wall);
					if(wall){
						//Debug.Log(wall.canPlaceObject(hit[e].normal));
						if(wall.canPlaceObject(hit[e].normal)){
							placeWall = wall;
							minDistance = hit[e].distance;
							pos = e;
						}
					}
				}
			}
			
		}
		
		if(placeWall){
			//Debug.Log("asdasdasd "+placeWall);
			bool des = true;
			if(_type >2) des = false;
			placeWall.placeObject(mirrors[_type], hit[pos].normal, des);

			quantity[_type] -= 1;
			buttons.setQuantity(quantity);

			sound.Play();
		}
	}

	private bool rotateMirror(){
		Vector2 origin = Camera.main.ScreenPointToRay(Input.mousePosition).origin;
		Collider2D hit = Physics2D.OverlapPoint(origin, 1 << LayerMask.NameToLayer ("MirrorClick"));
		if(hit){
			//hit.transform.parent.transform.localScale = new Vector3(-1,1,1);
			if(!hit.CompareTag("Wall")){
				hit.transform.parent.transform.Rotate(new Vector3(0,1,0)*180);
			}

			sound.Play();
			return true;
		}

		return false;
	}

	private void removeMirror(){
		//Debug.Log("remove");
		Vector2 origin = Camera.main.ScreenPointToRay(Input.mousePosition).origin;
		Collider2D hit = Physics2D.OverlapPoint(origin, 1 << LayerMask.NameToLayer ("MirrorClick"));

		if(hit){

			addQuantity(hit.transform.parent.gameObject);

			Destroy(hit.transform.parent.gameObject);

			eraseSound.Play();
		}

	}

	private void addQuantity(GameObject obj){

		int layer = obj.layer;

		if(layer == LayerMask.NameToLayer("Mirror")) quantity[0] += 1;
		if(layer == LayerMask.NameToLayer("Mirror45")) quantity[1] += 1;
		if(layer == LayerMask.NameToLayer("Mirror25")) quantity[2] += 1;
		
		if(layer == LayerMask.NameToLayer("Wall")){
			
			quantity[3] += 1;
			foreach(Transform child in obj.transform){
				addQuantity(child.gameObject);
			}
		}
		
		if(layer == LayerMask.NameToLayer("WallTransparent")){
			
			quantity[4] += 1;
			foreach(Transform child in obj.transform){
				addQuantity(child.gameObject);
			}
		}

		buttons.setQuantity(quantity);
	}

	public override void setType(int type){
		base.setType(type);

		if(type == -2) GetComponent<CameraMover>().active = true;
		else GetComponent<CameraMover>().active = false;
	}

	public void setQuantity(int[] quantity){
		this.quantity = quantity;

		buttons.setQuantity(quantity, true);
	}

	void OnDrawGizmosSelected() {

		Gizmos.color = Color.green;
		Gizmos.DrawSphere(Camera.main.ScreenPointToRay (Input.mousePosition).origin, 1);
	}
}
