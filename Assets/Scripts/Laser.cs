﻿using UnityEngine;
using System.Collections;


public class Laser : MonoBehaviour {

	public float imageSpeed = 0.2f;

	public LayerMask layerMaskNorm;
	public LayerMask layerMaskEsp;

	private Renderer _rend;
	private RaycastHit2D[] _rayHit;
	private bool _extend = true;

	private GameObject _reflect;

	private int _color;
	private LaserReceptor _receptor;

	public int hit = 0;

	private LayerMask layerMask;

	private GameObject _lastHit;

	void Awake(){
		
		_rend = GetComponentInChildren<Renderer> ();
	}

	void Start () {

		//_rend.material.mainTextureScale = new Vector2 (_rend.transform.localScale.x, 1);

		transform.GetChild(0).position = transform.position + (transform.right * (_rend.transform.localScale.x/2));
		//transform.GetChild(0).Translate(transform.right * (_rend.bounds.size.x/2));

		if(this.transform.GetChild(1).gameObject.layer != LayerMask.NameToLayer("Laser")){
			layerMask = layerMaskEsp;
			transform.Translate(new Vector3(0,0,-9));
		}
		else layerMask = layerMaskNorm;

		hit = 0;
	}
	
	// Update is called once per frame
	void Update () {
		_rayHit = Physics2D.RaycastAll (this.transform.position+transform.right*0.1f, transform.right, 10000, layerMask);

		//Debug.Log("length: " + _rayHit.Length);
//		Debug.Log("hit: " + hit);
		
		//Debug.DrawLine (transform.position+transform.right*0.1f, transform.position + transform.right * (_rend.transform.lossyScale.x));

		if(hit < 0) hit = _rayHit.Length;

		if(hit < _rayHit.Length){

			if (_rayHit[hit]) {

				//Debug.Log("hit: " + hit + " length: " + hit);
				//Debug.Log("hit: " + hit + " - " + _rayHit[hit].transform.gameObject.layer);

				if(_rayHit[hit].transform.CompareTag("Mirror")){

					//Debug.Log( gameObject.name + " , " + _color + " , mirror");

					createReflect();
					unActiveReceptor();
				}else if(_rayHit[hit].transform.CompareTag("ColorConverter")){
						
					//Debug.Log( gameObject.name + " COLOR" + " h: " + hit);
					if(_rayHit[hit].transform.GetComponent<ObjectData>().color == _color || 
					   transform.GetChild(1).gameObject.layer == LayerMask.NameToLayer("Inverser") || 
					   transform.GetChild(1).gameObject.layer == LayerMask.NameToLayer("LaserMirror")){

						hit++;
						//Debug.Log( gameObject.name + " h: " + hit);
					}else{
						CreateColorChild();
						unActiveReceptor();
					}
					
				}else if(_rayHit[hit].transform.gameObject.layer == LayerMask.NameToLayer("LaserMirror")){
					
					createReflect(false);					
					
				}else if(_rayHit[hit].transform.gameObject.layer == LayerMask.NameToLayer("Inverser")){

					createReflect(true);
					//CreateColorChild();
					//unActiveReceptor();

						
				}else{
					destroyReflect();

					if(_rayHit[hit].transform.CompareTag("Receptor")){
						if(_receptor){
							if(!_receptor.Equals(_rayHit[hit].transform.GetComponent<LaserReceptor>())){
								unActiveReceptor();
							}
						}

						_receptor = _rayHit[hit].transform.GetComponent<LaserReceptor>();
						_receptor.activate(_color, this.gameObject);
					}else if(_receptor){
						unActiveReceptor();
					}
				}

				
				transform.localScale = new Vector3 ((_rayHit[hit].distance+0.1f)/_rend.transform.localScale.x, 1, 1);
				_extend = false;
				
				//Debug.Log("---hit: " + hit + " - " + _rayHit[hit].transform.gameObject.layer);
			} else {
				_extend = true;

				destroyReflect();

				//hit = 0;
			}

			
		}else{
			hit = 0;
		}


		if (_extend) {
			transform.localScale += new Vector3 (10000, 0, 0) * Time.deltaTime;
		}


		_rend.material.mainTextureScale = new Vector2 (_rend.transform.lossyScale.x, 1);
		_rend.material.mainTextureOffset = new Vector2 (_rend.material.mainTextureOffset.x - imageSpeed, 0);
		
		//Debug.Log( gameObject.name + " h: " + hit);
		//Debug.Log( "------------");
	} 

	void OnDestroy() {
		destroyReflect ();

		if(_receptor){
			_receptor.unActive(_color);
			_receptor = null;
		}
	}
	
	private void createReflect( bool inverse = false){

		if(_rayHit[hit].transform.gameObject != _lastHit){
			destroyReflect();
			_lastHit = _rayHit[hit].transform.gameObject;
		}


		if (!_reflect) {
			_reflect = Instantiate (this.gameObject);
			_reflect.transform.localScale = new Vector3 (1, 1, 1);
			_reflect.GetComponent<Laser>().setColor(_color);
			_reflect.GetComponent<Laser>().hit = 0;

			//shit = 0;

		}

		if(_reflect.GetComponent<Laser>()._color != _color){
			_reflect.GetComponent<Laser>().setColor(_color);
		}
		_reflect.transform.position = _rayHit[hit].point;
		_reflect.transform.position += new Vector3(0,0,transform.position.z);

		if(inverse){			
			_reflect.transform.right = Vector2.Reflect (transform.right, inverseNormal(_rayHit[hit].normal));
		}else{			
			_reflect.transform.right = Vector2.Reflect (transform.right, _rayHit[hit].normal);
		}



		if(_reflect.transform.rotation.y == 1){
			_reflect.transform.rotation = new Quaternion(0,0,1,0);
		}

		if(gameObject.layer == LayerMask.NameToLayer("Inverser")){
			_reflect.transform.GetChild(1).gameObject.layer = LayerMask.NameToLayer("Inverser");
		}

		
		//Debug.Log (_reflect.transform.forward );
	}

	private Vector2 inverseNormal( Vector2 normal){
		return new Vector2(normal.y, -normal.x);
	}

	private void CreateColorChild(){
		if(!_reflect){	
			_reflect = Instantiate (this.gameObject);
			_reflect.transform.localScale = new Vector3 (1, 1, 1);
			_reflect.GetComponent<Laser>().setColor(_rayHit[hit].transform.GetComponent<ObjectData>().color);
	
		}

		_reflect.transform.position = _rayHit[hit].point;
		//_reflect.transform.position += transform.right;
		//Debug.Log(transform.name + " - " + transform.rotation);
		_reflect.transform.right = transform.right;

		if(_reflect.transform.rotation.y == 1){
			_reflect.transform.rotation = new Quaternion(0,0,1,0);
		}


			//_reflect.GetComponent<Laser>().hit = 0;

	}

	private void unActiveReceptor(){
		if(_receptor){
			_receptor.unActive(_color);
			_receptor = null;
		}
	}

	private void destroyReflect(){

		if (_reflect) {
			hit = 0;

			GameObject.Destroy(_reflect);
		}
	}

	public void setColor(Texture2D texture, int color){

		if(color == 5) this.transform.GetChild(1).gameObject.layer = LayerMask.NameToLayer("Inverser");
		if(color == 6) this.transform.GetChild(1).gameObject.layer = LayerMask.NameToLayer("LaserMirror");


		_rend.material.mainTexture = texture;
		_color = color;
	}

	public void setColor(int color){
		_rend.material.mainTexture = GlobalVariables.laserTextures[color];
		_color = color;
	} 

	public int getColor(){
		return _color;
	}
}
