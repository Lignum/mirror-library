﻿using UnityEngine;
using System.Collections;

public class GlassScr : ObjectData {
	
	public Texture2D baseText;
	private static Sprite[] baseTextures;

	// Use this for initialization
	void Awake () {		
		
		if(baseText){
			baseTextures = Resources.LoadAll<Sprite>("Images/"+baseText.name);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void setColor(int color){
		base.setColor(color);

		
		GetComponent<SpriteRenderer>().sprite = baseTextures[color+18];
	}
}
