﻿using UnityEngine;
using System.Collections;

public class BlockScr : ObjectData {

	
	public Texture2D text;
	private Sprite[] textures;

	// Use this for initialization
	void Awake () {
		
		if(text){
			textures = Resources.LoadAll<Sprite>("Images/"+text.name);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void setColor(int color){
		base.setColor(color);

		GetComponent<SpriteRenderer>().sprite = textures[color+14];

		transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = textures[color+14+6];

	}
}
