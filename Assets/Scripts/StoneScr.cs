﻿using UnityEngine;
using System.Collections;

public class StoneScr : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(!isRamp()){

			transform.up = Vector3.up;
		}else{
			Vector3 pos = transform.up;

			if(!transform.parent.CompareTag("Stone")){
				transform.position -= pos*0.5f;
			}
			
			WallScr wall = GetComponent<WallScr>();
			
			wall.up = false;
			wall.down = false;
			wall.left = false;
			wall.right = false;
			
			
			wall.rampUpLeft = true;
			wall.rampUpRight = true;
			wall.rampDownLeft = true;
			wall.rampDonwRight = true;
		}


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private bool isRamp(){
		
		//Debug.Log(transform.up == new Vector3(-1,-1,0).normalized);
		if(transform.up == new Vector3(-1,-1,0).normalized) return true;
		if(transform.up == new Vector3(-1,1,0).normalized) return true;
		if(transform.up == new Vector3(1,-1,0).normalized) return true;
		if(transform.up == new Vector3(1,1,0).normalized) return true;
		return false;
	}
}
