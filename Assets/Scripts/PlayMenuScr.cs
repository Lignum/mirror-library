﻿using UnityEngine;
using System.Collections;


public class PlayMenuScr : MonoBehaviour {


	public bool options = true;
	public AdsMng ads;

	// Use this for initialization
	void Start () {
		options = true;

		//RequestBanner();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnEnable(){

		ads.showBanner(true);

		if(options){
			transform.GetChild(0).gameObject.SetActive(true);
			transform.GetChild(1).gameObject.SetActive(false);
		}else{
			transform.GetChild(0).gameObject.SetActive(false);
			transform.GetChild(1).gameObject.SetActive(true);
		}

	}

	void OnDisable(){
		ads.showBanner(false);
	}


}
