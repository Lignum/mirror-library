﻿using UnityEngine;
using System.Collections;

using System;
using System.Linq;

using System.Collections.Generic;

public class ButonMng : MonoBehaviour {

	public void exitGame(){
		Application.Quit();
	}

	public void changeScene(string scene){
		Application.LoadLevel(scene);
	}

	public void nextLevel(){

		GlobalVariables.levelNum++;
		Application.LoadLevel("Play");
	}

}
