﻿using UnityEngine;
using System.Collections;

public class BackScr : MonoBehaviour {

	public int key;

	private static Sprite[] backSheet;

	// Use this for initialization
	void Awake () {

		if(backSheet == null){
			backSheet = Resources.LoadAll<Sprite>("Images/backgroundSheet");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setKey(int key){
		this.key = key;
		
		this.gameObject.GetComponent<SpriteRenderer>().sprite = backSheet[this.key];
	}
}
