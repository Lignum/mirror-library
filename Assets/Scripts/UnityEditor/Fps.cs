﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Fps : MonoBehaviour {

	Text text;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	

		text.text = "FPS: " + ((int)(1.0f/Time.deltaTime)).ToString();
	}
}
