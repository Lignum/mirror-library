﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class SelecShelf : MonoBehaviour {
			
	private int shelf;
	private float initPos;
	private float nextPos;
	private float des;
	private int dir;
	// Use this for initialization
	void Start () {		
		
		dir = 0;
		
		shelf = 0;
		initPos = GetComponent<RectTransform>().anchoredPosition.x;
		des = -400;
		//changeShelf(0);

		setShelf(GlobalVariables.shelf);
	}
	
	// Update is called once per frame
	void Update () {
		move();
	}

	
	public void changeShelf(int shelfDir){
		
		dir = shelfDir;
		
		this.shelf += shelfDir;

		if(shelf < 0) shelf = 0;
		//if(shelf > 4) shelf = 4;
		if(shelf > 2) shelf = 2;

		nextPos = initPos + this.shelf * des;

		GlobalVariables.shelf = this.shelf;
		
	//	Debug.Log("shel: " + this.shelf + " - " + nextPos);
//		Debug.Log("pos: " + this.GetComponent<RectTransform>().anchoredPosition);
	}

	public void setShelf(int shelf){

		
		this.shelf += shelf;
		nextPos = initPos + this.shelf * des;
		
		GetComponent<RectTransform>().anchoredPosition = new Vector2(nextPos,0);

		dir = 0;
	}
	
	private void move(){

		switch(dir){
		case 1: 

			if(GetComponent<RectTransform>().anchoredPosition.x > nextPos){
				GetComponent<RectTransform>().Translate(new Vector2(-1,0) * 40f * Time.deltaTime);
			}else{
				GetComponent<RectTransform>().anchoredPosition = new Vector2(nextPos,0);
				dir = 0;
			}

			break;

		case -1:
			if(GetComponent<RectTransform>().anchoredPosition.x < nextPos){
				GetComponent<RectTransform>().Translate(new Vector2(1,0) * 40f * Time.deltaTime);
			}else{
				GetComponent<RectTransform>().anchoredPosition = new Vector2(nextPos,0);
				dir = 0;
			}
			break;

		}

	}
}
