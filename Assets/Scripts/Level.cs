﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Level{
	
	public int[,] map;
	public objectData[,] objectsMap;	
	public int[,] backMap;

	public int mirror, mirror45, mirror25, stone, transparent;


	[System.Serializable]
	public struct objectData{
		float w,x,y,z;
		float posX, posY;

		
		float detw,detx,dety,detz;

		public int color;
		public int type;

		public void init(Quaternion rot, Vector2 pos, int color, int type, Quaternion detailRot){
			w = rot.w;
			x = rot.x;
			y = rot.y;
			z = rot.z;
			
			detw = detailRot.w;
			detx = detailRot.x;
			dety = detailRot.y;
			detz = detailRot.z;

			posX = pos.x;
			posY = pos.y;

			this.color = color;
			this.type = type;
		}

		public Quaternion rot(){
			return new Quaternion(x,y,z,w);
		}
		public Vector2 pos(){
			return new Vector2(posX, posY);
		}
		public Quaternion detailRot(){
			return new Quaternion(detx,dety,detz,detw);
		}
	}
	//public GameObject[,] backMap;

	public Level(int width, int height, GameObject[,] m, GameObject[,] o, GameObject[,] b){
		map = new int[width+1, height+1];
		objectsMap = new objectData[width+1, height+1];
		backMap = new int[width+1, height+1];

		int i=0;
		int j=0;

		//Debug.Log("w " + width + "h " + height);
		
		while(j<=height){
			
			//Debug.Log("i " + i + "j " + j);
			if(m[i,j] != null){
				map[i,j] = m[i,j].GetComponent<WallScr>().key;
			}else{
				map[i,j] = -1;
			}

			
			if(b[i,j] != null){
				backMap[i,j] = b[i,j].GetComponent<BackScr>().key;
			}else{
				backMap[i,j] = -1;
			}

			if(o[i,j] != null){
				//LaserBase l = o[i,j].GetComponent<LaserBase>();
				if(o[i,j].GetComponent<ObjectData>()){

					int color = o[i,j].GetComponent<ObjectData>().color;

					int type = o[i,j].GetComponent<ObjectData>().type;

					objectsMap[i,j].init(o[i,j].transform.rotation, o[i,j].transform.position, color, type, o[i,j].transform.GetChild(0).rotation);
				//Debug.Log(objectsMapPos[i,j].rot());
				}else{
					if(o[i,j].layer == LayerMask.NameToLayer("Mirror")) mirror++;
				    if(o[i,j].layer == LayerMask.NameToLayer("Mirror45")) mirror45++;
				    if(o[i,j].layer == LayerMask.NameToLayer("Mirror25")) mirror25++;
				    if(o[i,j].layer == LayerMask.NameToLayer("Wall")) stone++;
				    if(o[i,j].layer == LayerMask.NameToLayer("WallTransparent")) transparent++;
					
					objectsMap[i,j].color = -1;
					//Debug.Log(o[i,j].layer);
				}
			}else{
				objectsMap[i,j].color = -1;
			}

			if(i>=width){
				i=0;
				j++;
			}else{
				i++;
			}
			
		}
		//backMap = new GameObject[maxWidth, maxHeight];
	}

}
