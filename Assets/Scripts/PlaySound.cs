﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {

	AudioSource clip;

	// Use this for initialization
	void Start () {
		clip = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void play(){
		clip.Play();
	}
}
