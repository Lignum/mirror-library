﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadLevelList : MonoBehaviour {

	public GameObject levelbutton;

	// Use this for initialization
	void Start () {

		refresLevelList();
		//Debug.Log (levels.Length);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void refresLevelList(){
		removeLevelList();

		GameObject button;
		
		TextAsset[] levels = Resources.LoadAll<TextAsset>("Levels");

		foreach(TextAsset t in levels){
			button = (GameObject)Instantiate(levelbutton);
			button.transform.SetParent(transform);
			button.transform.localScale = new Vector3(1,1,1);

			button.GetComponentInChildren<Text>().text = t.name;
		}
	}

	private void removeLevelList(){
		foreach(Transform child in transform){
			Destroy(child.gameObject);
		}
	}

	public void checkButtons(GameObject button){

		foreach(Transform child in transform){
			if(child != button.transform){
				child.GetComponent<btnLevelSelector>().unCheck();
			}
		}

	}
}
