﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class btnLevelSelector : MonoBehaviour {

	private bool check;

	public Sprite checkSprite;

	// Use this for initialization
	void Start () {
		check = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void click(){
		GetComponent<Button>().image.overrideSprite = checkSprite;
		check = true;

		GetComponentInParent<LoadLevelList>().checkButtons(this.gameObject);
	}

	public void unCheck(){
		check = false;
		GetComponent<Button>().image.overrideSprite = null;
	}

	public bool isChecked(){
		return check;
	}
}
