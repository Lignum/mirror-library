﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CheckButtons : MonoBehaviour {

	//public GameObject manager;
	public Manager _manager;

	public GameObject check;
	public GameObject buttonBase;
	
	public GameObject moveCameraButton;
	public GameObject eraseButton;
		
	public Sprite[] buttonTextures;
	public int[] quantity;

	private GameObject[] buttons;

	private GameObject _che;

	// Use this for initialization
	void Start () {

		//_manager = manager.GetComponent<Manager>();

		_che = (GameObject)Instantiate(check);

		if(quantity.Length < buttonTextures.Length){
			int[] aux = new int[buttonTextures.Length];
			for(int i=0; i<aux.Length; i++){
				if(i<quantity.Length) aux[i] = quantity[i];
				else aux[i] = 0;
			}

			quantity = aux;

		}

		createButtons();

		//click(buttons[0], 0);
		_che.transform.SetParent(buttons[0].transform);
		_che.transform.localPosition = new Vector3(0,0,0);

		_manager.setType(0);
		_che.transform.localScale = new Vector3(1,1,1);

		_che.GetComponent<RectTransform>().sizeDelta = GetComponent<GridLayoutGroup>().cellSize;

	}

	
	// Update is called once per frame
	void Update () {
	}

	public void click(GameObject button, int type){

		bool c = false;

		if(type <= -1) c = true;
		else if(quantity[type] != 0) c = true;

		if(c){			
			
			_che.transform.SetParent(button.transform);
			_che.transform.localPosition = new Vector3(0.421f,0,0);
			
			_manager.setType(type);
		}

	}

	private void createButtons(){

		int plus = 0;

		if(eraseButton != null) plus++;
		if(moveCameraButton != null) plus++;

		buttons = new GameObject[buttonTextures.Length+plus];

		for(int i=0; i<buttons.Length-plus; i++){

			if(quantity[i] != -1){
				
				buttons[i] = (GameObject)Instantiate(buttonBase);
				buttons[i].transform.SetParent(this.transform);
				buttons[i].transform.localScale = new Vector3(1,1,1);

				buttons[i].GetComponent<Button>().image.overrideSprite = buttonTextures[i];

				if(quantity[i] == -2) buttons[i].GetComponentInChildren<Text>().text = "";
				else buttons[i].GetComponentInChildren<Text>().text = "x " + quantity[i].ToString();
							
				buttons[i].GetComponent<ButtonCheck>().type = i;
			}
		}


		
		if(eraseButton != null){
			//Erase button
			buttons[buttons.Length-plus] = (GameObject)Instantiate(eraseButton);
			buttons[buttons.Length-plus].transform.SetParent(this.transform);
			buttons[buttons.Length-plus].transform.localScale = new Vector3(1,1,1);
			plus--;
		}
		
		if(moveCameraButton != null){
			//Move Camera button
			buttons[buttons.Length-plus] = (GameObject)Instantiate(moveCameraButton);
			buttons[buttons.Length-plus].transform.SetParent(this.transform);
			buttons[buttons.Length-plus].transform.localScale = new Vector3(1,1,1);
			plus--;
		}
	}

	
	public void setQuantity(int[] quantity, bool remove = false){
		this.quantity = quantity;

		bool start = true;

		for(int i=0; i<transform.childCount-1; i++){

			if(transform.GetChild(i).GetChild(0).gameObject.activeInHierarchy){
				transform.GetChild(i).GetComponentInChildren<Text>().text = "x " + quantity[i].ToString();

			}
			if(remove){
				if(quantity[i] == 0){
					transform.GetChild(i).GetComponent<Image>().color = new Color(1,1,1,0.2f);
					transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
					//transform.GetChild(i).gameObject.SetActive(false);
				}else if(start){
					start = false;
					click(transform.GetChild(i).gameObject, i);
				}
			}
		}

	}
}
