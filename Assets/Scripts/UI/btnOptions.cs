﻿using UnityEngine;
using System.Collections;

public class btnOptions : MonoBehaviour {

	public GameObject optionMenu;

	public AudioSource sound;

	// Use this for initialization
	void Start () {
		optionMenu.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void showHideOptionMenu(){

		//if(optionMenu.GetComponent<PlayMenuScr>().options == false){

			sound.Play();

			if(optionMenu.activeSelf){

				PlayMenuScr menu = optionMenu.GetComponent<PlayMenuScr>();
				if(menu) optionMenu.GetComponent<PlayMenuScr>().options = true;

				optionMenu.SetActive(false);
			}else{
				optionMenu.SetActive(true);
			}
		//}

	}
}
