﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
public class VersionControler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		TextAsset version = Resources.Load<TextAsset>("Data/Version");

		GetComponent<Text>().text = version.text;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
