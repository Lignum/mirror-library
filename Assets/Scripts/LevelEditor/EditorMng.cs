﻿using UnityEngine;
using System.Collections;

using System.Text.RegularExpressions;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.EventSystems;

public class EditorMng : Manager {

	//public Object[] walls;

	public GameObject wall;
	public GameObject wallRamp;
	public GameObject back;
	public GameObject laser;
	public GameObject laserReceptor;
	public GameObject shelf;
	public GameObject wallMirror;
	public GameObject glass;
	public GameObject backWall;
	
	public GameObject mirror;
	public GameObject mirror45;
	public GameObject mirror25;	
	public GameObject stone;
	public GameObject transparent;

	public GameObject[] Panels;
	private int _panel;

	private const int _maxHeight = 27;
	private const int _maxWidth = 40;
	
	private const int _minHeight = 14;
	private const int _minWidth = 21;
	
	public int actualHeight;
	public int actualWidth;


	public GameObject[,] map;

	private Text _widht, _height;

	public EditorLaserMng _laserMng;
	public EditorBackMng _backMng;


	// level, wall, backgorund, objects
	private GameObject l,w,b,o;

	// Use this for initialization
	void Start () {

		createBaseObjects();

		_laserMng = new EditorLaserMng(laser, laserReceptor, shelf, wallMirror, glass, mirror, mirror45, mirror25, stone, transparent);
		_backMng = new EditorBackMng(back);
		//walls = Resources.LoadAll("Prefabs/Walls");
		//Debug.Log(walls.Length);

		changePanel(0);

		
		initMaps();
		
		
		_widht = GameObject.Find("txtWidht").GetComponent<Text>();
		_height = GameObject.Find("txtHeight").GetComponent<Text>();
		
		_widht.text = actualWidth.ToString();		
		_height.text = actualHeight.ToString();

		createBorders();
		
		GetComponent<CameraMover>().setMaxPos(actualWidth, actualHeight);

	}

	private void initMaps(){
		map = new GameObject[_maxWidth, _maxHeight];
		_backMng.backMap = new GameObject[_maxWidth, _maxHeight];
		_laserMng.objectsMap = new GameObject[_maxWidth, _maxHeight];
		
		actualHeight = _minHeight;
		actualWidth = _minWidth;
		
		
		_laserMng.setMaxWidhtHeight(actualWidth, actualHeight);
		
		int i=0;
		int j=0;
		
		
		while(j<actualHeight){
			
			map[i,j] = null;
			_backMng.backMap[i,j] = null;
			_laserMng.objectsMap[i,j] = null;
			
			if(i>actualWidth){
				i=0;
				j++;
			}else{
				i++;
			}
			
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		if(!EventSystem.current.IsPointerOverGameObject()){
			switch(_panel){
			case 0: // walls
				wallClick();
				break;

			case 1: // Lasers
			case 3:
				_laserMng.click(o);
				break;
			case 2: // Background
				_backMng.click(b);
				break;
			}
		}

	
	}


	private void wallClick(){
		if(Input.GetMouseButton(0)){
			Vector2 pos = Camera.main.ScreenPointToRay (Input.mousePosition).origin;
			
			int i = (int)Mathf.Round(pos.x);
			int j = (int)Mathf.Round(pos.y);
			
			switch(_type){
			case -1:
				destroyWall(i,j);
				break;
			case 0:
				createWall(i,j);
				break;
			}

			if(_type >= 1){
				createRamp(i,j);
			}
		}
	}

	private void createBaseObjects(){

		l = new GameObject();
		w = new GameObject();
		b = new GameObject();
		o = new GameObject();

		w.transform.parent = l.transform;
		b.transform.parent = l.transform;
		o.transform.parent = l.transform;

		l.name = "Level";
		w.name = "Walls";
		b.name = "Background";
		o.name = "Objects";

	}


	public void changeWidth(int x){

		if(actualWidth+x >= _minWidth && actualWidth+x < _maxWidth){
		
			//moveBorders(x,0);
			setXBorder(actualWidth+x);
			actualWidth += x;

			_widht.text = actualWidth.ToString();

			for(int j=0; j<=actualHeight; j++){
				if(x<0) updateAroundWall(actualWidth, j);
				else updateAroundWall(actualWidth-x, j);
			}
		}
		
		_laserMng.setMaxWidhtHeight(actualWidth, actualHeight);
		GetComponent<CameraMover>().setMaxPos(actualWidth, actualHeight);
	}

	public void changeHeight(int y){

		if(actualHeight+y >= _minHeight && actualHeight+y < _maxHeight){

			//moveBorders(0,y);
			setYBorder(actualHeight+y);
			actualHeight += y;

			_height.text = actualHeight.ToString();

			for(int j=0; j<=actualWidth; j++){
				if(y<0) updateAroundWall(j, actualHeight);
				else updateAroundWall(j, actualHeight-y);
			}
		}
		
		_laserMng.setMaxWidhtHeight(actualWidth, actualHeight);
		GetComponent<CameraMover>().setMaxPos(actualWidth, actualHeight);
	}

	private void createWall(int x, int y, bool ignoreBorders = false){

		if((x>=0 && y>=0 && x<=actualWidth && y<=actualHeight) || ignoreBorders){

			if(map[x, y] == null){

				//GameObject wall = getwallFromKey(blockToKey(x,y));

				//if(wall != null){
				map[x, y] = (GameObject)Instantiate(wall, new Vector2(x,y), Quaternion.identity);
				map[x,y].GetComponent<WallScr>().setKey(blockToKey(x,y));
				updateAroundWall(x,y);

				map[x,y].transform.parent = w.transform;
				//}
			}
		}

	}

	private void createRamp(int x, int y, bool ignoreBorders = false){
		
		if((x>=0 && y>=0 && x<=actualWidth && y<=actualHeight) || ignoreBorders){
			
			if(map[x, y] == null){
				
				map[x, y] = (GameObject)Instantiate(wallRamp, new Vector2(x,y), Quaternion.identity);
				map[x,y].GetComponent<WallRampScr>().setKey(59+_type);
				updateAroundWall(x,y);
				
				map[x,y].transform.parent = w.transform;
			}
		}
	}

	private void destroyWall(int x, int y, bool ignoreBorders = false, bool update = true){

		if((x>=1 && y>=1 && x<actualWidth && y<actualHeight) || ignoreBorders ){
			if(map[x, y] != null){

				Destroy(map[x,y]);
				map[x,y] = null;

				if(update){
					updateAroundWall(x,y);
				}
			}
		}
	}


	private void setXBorder(int x){
		for(int j=1; j<actualHeight; j++){

			if(map[actualWidth, j] != null){
				map[actualWidth, j].transform.position = new Vector2(x, j);
				map[x, j] = map[actualWidth, j];
				map[actualWidth, j] = null;
			}else{
				createWall(x,j);
			}
		}

		if(x>0){
			for(int i=actualWidth; i<=x; i++){				
				createWall(i,0, true);
				createWall(i, actualHeight, true);
			}
			//createWall(0,actualHeight+y, true);
			//createWall(actualWidth,actualHeight+y, true);
		}else{
			destroyWall(x, actualHeight, true);
			destroyWall(actualWidth,actualHeight, true);
		}
	}

	private void setYBorder(int y){
		for(int i=1; i<actualWidth; i++){

			if(map[i, actualHeight] != null){
				map[i, actualHeight].transform.position = new Vector2(i, y);
				map[i,y] = map[i, actualHeight];
				map[i, actualHeight] = null;
			}else{
				createWall(i,y);
			}			
		}

		if(y>0){
			for(int j=actualHeight; j<=y; j++){
				createWall(0,j, true);
				createWall(actualWidth,j, true);
			}
			//createWall(0,actualHeight+y, true);
			//createWall(actualWidth,actualHeight+y, true);
		}else{
			destroyWall(0,actualHeight, true);
			destroyWall(actualWidth,actualHeight, true);
		}
	}

	private void createBorders(){

		for(int i=1; i<actualWidth; i++){
			createWall(i,0);
			createWall(i,actualHeight);
		}

		for(int j=0; j<=actualHeight; j++){
			createWall(0,j);
			createWall(actualWidth,j);
		}
	
	}

	private void updateAroundWall(int x, int y){
		int i=x-1;
		int j=y-1;

		//GameObject wall = null;;

		
		while(j<=y+1){


			if(i==x && j==y) i++;
			if(i>=0 && i<_maxWidth && j>=0 && j<_maxHeight){
								
				//Debug.Log("i: "+ i + " j: " +j);

				if(map[i,j] != null){

					if(map[i,j].GetComponent<WallScr>().getKey()[4] == '1'){
						map[i,j].GetComponent<WallScr>().setKey(blockToKey(i,j));
					}
				}
			}

			if(i>=x+1){
				i=x-1;
				j++;
			}else{
				i++;
			}
		}
	}
	

	private string blockToKey(int x, int y){

		string s = "";

		int i=x-1;
		int j=y+1;
		
		while(j>=y-1){

			if(i>=0 && i<actualWidth && j>=0 && j<actualHeight){
				if(map[i,j] != null){ 
					s+=1;
				}else s += '0';
			}else{
				s += '1';
			}

			
			//Debug.Log("i: "+ i + " j: " +j);

			if(i>=x+1){
				i=x-1;
				j--;
			}else{
				i++;
			}
		}

		return s;

	}

	public override void setType(int type){
		base.setType(type);

		if(type == -2) GetComponent<CameraMover>().active = true;
		else GetComponent<CameraMover>().active = false;

		if(type>=100) changePanel(type-100);
//		Debug.Log(_type);
		switch(_panel){
		case 1:
			_laserMng.setType(_type);
			break;
		case 3:
			_laserMng.setType(_type+50);
			break;
		case 2:
			_backMng.setType(_type);
			break;
		}
	}

	private void changePanel(int panel){


		_panel = panel;
		for(int i=0; i<Panels.Length; i++){			
			Panels[i].SetActive(false);
		}
		
		Panels[panel].SetActive(true);

		_type = 0;

	}



	public void createLevel(Level l){
		int i=0;
		int j=0;
		
		actualWidth = l.map.GetLength(0)-1;
		actualHeight = l.map.GetLength(1)-1;
		GetComponent<CameraMover>().setMaxPos(actualWidth, actualHeight);
		//setXBorder(l.map.GetLength(0));
		//setYBorder(l.map.GetLength(1));

		//changeWidth(l.map.GetLength(0) - actualWidth);
		//changeHeight(l.map.GetLength(1) - actualHeight);
		//Debug.Log("w " + l.map.GetLength(0) + "h " + actualWidth);
		
		while(j<=l.map.GetLength(1)-1){
			
			//Debug.Log("i " + i + "j " + j);
			//Debug.Log("i " + l.map[i,j]);

			loadLevelWalls(i,j,l);

			loadLevelObjects(i,j,l);

			loadBackground(i,j,l);


			if(i>=l.map.GetLength(0)-1){
				i=0;
				j++;
			}else{
				i++;
			}
			
		}

		
	}

	private void loadLevelWalls(int i, int j, Level l){
		if(l.map[i,j] != -1){
			if(map[i,j] == null){
				//Debug.Log ("i: " + i + " j: " +j + " - " + l.map[i,j]);
				//createWall(i,j);
				
				if(l.map[i,j] >= 60){					
					map[i,j] = (GameObject)Instantiate(wallRamp, new Vector2(i,j), Quaternion.identity);
				}else{
					map[i,j] = (GameObject)Instantiate(wall, new Vector2(i,j), Quaternion.identity);
				}

				map[i,j].GetComponent<WallScr>().setKey(l.map[i,j]);
				map[i,j].transform.parent = w.transform;
				//updateAroundWall(i,j);
			}else{
				map[i,j].GetComponent<WallScr>().setKey(l.map[i,j]);
			}
		}else{
			destroyWall(i,j, true, true);
		}
	}

	private void loadLevelObjects(int i, int j, Level l){
		if(l.objectsMap[i,j].color != -1){
			if(_laserMng.objectsMap[i,j] == null){
				
				GameObject t = laser;

				switch(l.objectsMap[i,j].type){
				case 1: t = laserReceptor;
					break;
				case 2: t = shelf;
					break;
				case 3: t = wallMirror;
					break;
				case 4: t = glass;
					break;
				}
				
				GameObject obj = _laserMng.createObject(t, l.objectsMap[i,j].color, i,j);
				obj.transform.rotation = l.objectsMap[i,j].rot();
				obj.transform.position = l.objectsMap[i,j].pos();
				obj.transform.GetChild(0).rotation = l.objectsMap[i,j].detailRot();
				obj.transform.parent = o.transform;
			}
		}else{
			_laserMng.destroyObject(i,j);
		}
	}

	private void loadBackground(int i, int j, Level l){
		if(l.backMap[i,j] != -1){
			if(_backMng.backMap[i,j] == null){
				//Debug.Log ("i: " + i + " j: " +j + " - " + l.map[i,j]);
				//createWall(i,j);
				_backMng.backMap[i,j] = (GameObject)Instantiate(back, new Vector2(i,j), Quaternion.identity);
				_backMng.backMap[i,j].GetComponent<BackScr>().setKey(l.backMap[i,j]);
				_backMng.backMap[i,j].transform.parent = b.transform;
				//updateAroundWall(i,j);
			}else{
				_backMng.backMap[i,j].GetComponent<BackScr>().setKey(l.backMap[i,j]);
			}
		}else{
			_backMng.destroyObject(i,j);
			//destroyWall(i,j, true, true);
		}
	}
}
