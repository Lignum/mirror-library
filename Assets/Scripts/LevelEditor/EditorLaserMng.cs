﻿using UnityEngine;
using System.Collections;

public class EditorLaserMng {

	private int _type;

	public GameObject[,] objectsMap;

	private GameObject _laser;
	private GameObject _laserReceptor;
	private GameObject _shelf;
	private GameObject _wallMirror;
	private GameObject _glass;

	
	private GameObject _mirror;
	private GameObject _mirror45;
	private GameObject _mirror25;	
	private GameObject _stone;
	private GameObject _transparent;

	private int _width, _height;
	
	private Vector2 origin;
	private RaycastHit2D[] hit;

	private static int _numLasers = 5;

	public EditorLaserMng(GameObject laser, GameObject laserReceptor, GameObject shelf, 
	                      GameObject wallMirror, GameObject glass, 
	                      GameObject mirror, GameObject mirror45, GameObject mirror25, 
	                      GameObject stone, GameObject transparent){

		_laser = laser;
		_laserReceptor = laserReceptor;
		_shelf = shelf;
		_wallMirror = wallMirror;
		_glass = glass;

		_mirror = mirror;
		_mirror45 = mirror45;
		_mirror25 = mirror25;
		_stone = stone;
		_transparent = transparent;

		
		_type = 0;
		hit = new RaycastHit2D[8];
	}

	public void setType(int type){
		_type = type;
	}

	public void click(GameObject objParent){
		if(Input.GetMouseButtonDown(0)){
	
			if(_type == -1 || _type == 49)  destroyObject();
			else if(_type >= 0){
				if(canCreate()) createObject(objParent);
				else editObject();
			}
		}
	}

	public void setMaxWidhtHeight(int width, int height){
		_width = width;
		_height = height;
	}
	
	private void createObject(GameObject objParent){
		//Debug.Log("t: " +_type);
		Debug.Log("create");

		origin = Camera.main.ScreenPointToRay (Input.mousePosition).origin;
		
		int x = (int)Mathf.Round(origin.x);
		int y = (int)Mathf.Round(origin.y);

		if(x>0 && y>0 && x<_width && y<_height){
			
			launchRaycast();
			
			float minDistance = 100f;
			int pos = 0;
			WallScr wall = null;
			WallScr placeWall = null;
			
			for(int e=0; e<hit.Length; e++){
				if(hit[e].collider){
					if(hit[e].distance < minDistance){
						wall = hit[e].collider.gameObject.GetComponent<WallScr>();
						if(!wall){						
							wall = hit[e].transform.parent.GetComponent<WallScr>();
						}
						if(wall){
						//Debug.Log(hit[pos].collider.gameObject);
							if(wall.canPlaceObject(hit[e].normal)){
								Debug.Log("Can place");
								placeWall = wall;
								minDistance = hit[e].distance;
								pos = e;
							}
						}
					}
				}				
			}
			
			if(placeWall){

				Debug.Log("placeWall");

				int xx = (int)(hit[pos].collider.transform.position.x + hit[pos].normal.x);
				int yy = (int)(hit[pos].collider.transform.position.y + hit[pos].normal.y);

				//Debug.Log("Create: " + new Vector2(xx,yy));
				if(objectsMap[xx, yy] == null){
					if(_type<=4 || _type==19 || _type==20){
						objectsMap[xx, yy] = placeWall.placeObject(_laser,hit[pos].normal);
						objectsMap[xx, yy].GetComponent<LaserBase>().setColor(_type);
					}else if(_type<=9){
						objectsMap[xx, yy] = placeWall.placeObject(_laserReceptor,hit[pos].normal);
						objectsMap[xx, yy].GetComponent<LaserReceptor>().setColor(_type-_numLasers);
					}else if(_type <= 12){
						objectsMap[xx, yy] = placeWall.placeObject(_shelf,hit[pos].normal);
						int add = 0;
						if(_type == 12 && hit[pos].normal.y == -1) add = 2;
						objectsMap[xx, yy].GetComponent<BlockScr>().setColor(_type+add);
					}else if(_type == 13){
						objectsMap[xx, yy] = placeWall.placeObject(_wallMirror,hit[pos].normal);
					}else if(_type < 50){						
						objectsMap[xx, yy] = placeWall.placeObject(_glass,hit[pos].normal, false);
						objectsMap[xx, yy].GetComponent<GlassScr>().setColor(_type-14);
					}else if(_type == 50){
						objectsMap[xx, yy] = placeWall.placeObject(_mirror,hit[pos].normal);
					}else if(_type == 51){
						objectsMap[xx, yy] = placeWall.placeObject(_mirror45,hit[pos].normal);
					}else if(_type == 52){
						objectsMap[xx, yy] = placeWall.placeObject(_mirror25,hit[pos].normal);
					}else if(_type == 53){
						objectsMap[xx, yy] = placeWall.placeObject(_stone,hit[pos].normal, false);
					}else if(_type == 54){
						objectsMap[xx, yy] = placeWall.placeObject(_transparent,hit[pos].normal, false);
					}

					objectsMap[xx, yy].transform.parent = objParent.transform;

				}
				
			}

		}
		
	}

	private void launchRaycast(){
		int i=-1;
		int j=-1;
		
		int count = 0;


		while(j<=1){
			if(i==0 && j==0) i++;
			
			hit[count] = Physics2D.Raycast(origin, new Vector3(i,j,0), 1.0f, 1 << LayerMask.NameToLayer ("Wall") | 1 << LayerMask.NameToLayer("WallNoMirror") | 1 << LayerMask.NameToLayer("WallTransparent") );
			count++;
			
			if(i>=1){
				i=-1;
				j++;
			}else{
				i++;
			}
			
			
		}

		
		//Debug.Log("----");
	}

	public GameObject createObject(GameObject obj, int color ,int x, int y){

		objectsMap[x, y] = (GameObject)GameObject.Instantiate(obj, new Vector2(x,y), Quaternion.identity);
		objectsMap[x, y].GetComponent<ObjectData>().setColor(color);

		return objectsMap[x,y];
	}

	private void destroyObject(){

		Vector2 pos = objectClicked();

		if(pos.x >-100){
			GameObject.Destroy(objectsMap[(int)pos.x, (int)Mathf.Round(pos.y)]);
			objectsMap[(int)pos.x, (int)Mathf.Round(pos.y)] = null;
		}

	}

	public void destroyObject(int x, int y){		
		GameObject.Destroy(objectsMap[x,y]);
		objectsMap[x,y] = null;
	}

	private void editObject(){
		
		Vector2 pos = objectClicked();

		if(pos.x >-100){
			if(objectsMap[(int)pos.x, (int)Mathf.Round(pos.y)].CompareTag("Base")){
				objectsMap[(int)pos.x, (int)Mathf.Round(pos.y)].GetComponent<LaserBase>().rotate();
			}else if(objectsMap[(int)pos.x, (int)Mathf.Round(pos.y)].CompareTag("Mirror")){
				objectsMap[(int)pos.x, (int)Mathf.Round(pos.y)].transform.Rotate(new Vector3(0,1,0)*180);
				//objectsMap[(int)pos.x, (int)Mathf.Round(pos.y)].GetComponent<LaserBase>().rotate();
			}
		}
	}

	private Vector2 objectClicked(){
		Vector2 origin = Camera.main.ScreenPointToRay(Input.mousePosition).origin;		
		origin += new Vector2(0.5f, 0.5f);

		//Debug.Log("origin:" + origin);
		//Debug.Log("origin2:" + (int)Mathf.Ceil(origin.x) + " - " + (int)Mathf.Ceil(origin.y));
		
		int xx = (int)Mathf.Floor(origin.x);
		int yy = (int)Mathf.Floor(origin.y);

		if(objectsMap[xx, yy] != null){
			return new Vector2(xx, yy);
		}

		return new Vector2(-100, -100);
	}

	private bool canCreate(){
		Vector2 origin = Camera.main.ScreenPointToRay(Input.mousePosition).origin;
		Collider2D hit = Physics2D.OverlapPoint(origin, (1 << LayerMask.NameToLayer ("ObjectClick")) | (1 << LayerMask.NameToLayer("MirrorClick")));
		if(hit)return false;

		return true;
	}
}
