﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class LevelEditorLoader : MonoBehaviour {

	public string levelName = "000";

	public GameObject editorMng;
	public GameObject levelSelector;

	private int actualWidth, actualHeight;
	private GameObject[,] map;
	private GameObject[,] objectsMap;
	private GameObject[,] backMap;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void saveLevel(){

		levelName = GetComponentInChildren<InputField>().text;

		//Debug.Log ("save");
		
		actualWidth = editorMng.GetComponent<EditorMng>().actualWidth;
		actualHeight = editorMng.GetComponent<EditorMng>().actualHeight;
		map = editorMng.GetComponent<EditorMng>().map;
		objectsMap = editorMng.GetComponent<EditorMng>()._laserMng.objectsMap;
		backMap = editorMng.GetComponent<EditorMng>()._backMng.backMap;

		
		Level level = new Level(actualWidth, actualHeight, map, objectsMap, backMap);

		//Stream stream = File.Open(Application.streamingAssetsPath + "/prueba.lev", FileMode.Create);
		Stream stream = File.Open(GlobalVariables.resDirectory + GlobalVariables.levelsDirectory + levelName +".txt", FileMode.Create);
		
		BinaryFormatter bformatter = new BinaryFormatter();
		
		bformatter.Serialize(stream, level);
		stream.Close();

		levelSelector.GetComponent<LoadLevelList>().refresLevelList();
		
		//Debug.Log("Save fin");
	}
	
	public void loadEditorlevel(){
		//Debug.Log ("childs: " + levelSelector.transform.childCount);
		Transform child;

		for(int i=0; i<levelSelector.transform.childCount; i++){
			child = levelSelector.transform.GetChild(i);
			if(child.GetComponent<btnLevelSelector>().isChecked()){
				levelName = child.GetComponentInChildren<Text>().text;
				break;
			}
		}

		
		Level level = null;
		
		TextAsset text = Resources.Load<TextAsset>(GlobalVariables.levelsDirectory + levelName);
		Stream stream = new MemoryStream(text.bytes);
		
		//BinaryReader br = new BinaryReader(s);
		
		BinaryFormatter bformatter = new BinaryFormatter();
		level = (Level)bformatter.Deserialize(stream);
		
		//Debug.Log(text);
		
		stream.Close();
		
		//Debug.Log("load fin");

		editorMng.GetComponent<EditorMng>().createLevel(level);
		//createLevel(level);
	}

}
