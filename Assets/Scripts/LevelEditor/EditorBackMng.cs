﻿using UnityEngine;
using System.Collections;

public class EditorBackMng {

	
	public GameObject[,] backMap;

	private int _type;
	private GameObject _back;

	public EditorBackMng(GameObject back){
		_back = back;

		_type = 0;
	}
	
	public void setType(int type){
		_type = type;
	}

	public void click(GameObject objParent){
		
		Vector2 pos = Camera.main.ScreenPointToRay (Input.mousePosition).origin;
		
		int i = (int)Mathf.Round(pos.x);
		int j = (int)Mathf.Round(pos.y);

		if(Input.GetMouseButtonDown(0)){

			if(_type != -1){
				createObject(i,j, objParent);
			}else{
				destroyObject(i,j);
			}
		}else if(Input.GetMouseButtonDown(1)){			
			destroyObject(i,j);
		}
	}

	public void destroyObject(int x, int y){
		if(backMap[x, y] != null){
			 
			GameObject.Destroy(backMap[x,y]);
			backMap[x,y] = null;
		}
	}

	private void createObject(int x, int y, GameObject objParent){
		//if(x>=0 && y>=0 && x<=actualWidth && y<=actualHeight){
			
		if(backMap[x, y] == null){

			backMap[x, y] = (GameObject)GameObject.Instantiate(_back, new Vector2(x,y), Quaternion.identity);
			
			backMap[x,y].transform.parent = objParent.transform;

			backMap[x,y].GetComponent<BackScr>().setKey(_type);
		}
		//}
	}
}
