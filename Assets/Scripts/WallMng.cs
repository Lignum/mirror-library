﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class WallMng : MonoBehaviour {

	public Texture2D t;
	public Sprite[] wallSheet;

	public string[] keys;

	// Use this for initialization
	void Awake () {

		wallSheet = Resources.LoadAll<Sprite>("Images/"+t.name);

		
		/*for(int i=0; i<keys.Length; i++){
			
			//keys[i] = keys[i].Replace("1","[1|R]");
			Debug.Log("k: " + keys[i]);
		}*/

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	
	public void changeButtonKey(WallScr button, int key){
		button.gameObject.GetComponent<SpriteRenderer>().sprite = wallSheet[key];

		button.key = key;

		changeButtonMirrorPlace(button, keys[key]);
	}

	public void changeButtonKey(WallScr button, string key){
		//Debug.Log("k: " + key);


		for(int i=0; i<keys.Length; i++){

			if(System.Text.RegularExpressions.Regex.IsMatch(key, keys[i].Replace("1","[1|r]"))){
				//Debug.Log("k2: " + keys[i]);
				changeButtonKey(button, i);
				break;
			}
		}

	}

	private void changeButtonMirrorPlace(WallScr button, string key){

		if(key.Length == 9){

			if(key[4] == 'R'){
				if(key[1] == '0' && key[3] == '0') button.rampUpLeft = true;
				else button.rampUpLeft = false;
				if(key[1] == '0' && key[5] == '0') button.rampUpRight = true;
				else button.rampUpRight = false;
				if(key[7] == '0' && key[3] == '0') button.rampDownLeft = true;
				else button.rampDownLeft = false;
				if(key[7] == '0' && key[5] == '0') button.rampDonwRight = true;
				else button.rampDonwRight = false;

			}else{

				if(key[1] == '0') button.up = true;
				else button.up = false;
				if(key[3] == '0') button.left = true;
				else button.left = false;
				if(key[5] == '0') button.right = true;
				else button.right = false;
				if(key[7] == '0') button.down = true;
				else button.down = false;
			}
		}
	}
}
