﻿using UnityEngine;
using System.Collections;

public class LaserBase : ObjectData {

	public Texture2D baseText;
	private static Sprite[] baseTextures;
	public Texture2D[] laserTextures;

	private int _rot = 0;

	void Awake(){

		if(baseText){
			baseTextures = Resources.LoadAll<Sprite>("Images/"+baseText.name);
			
			GlobalVariables.laserTextures = laserTextures;
		}
	}
	// Use this for initialization
	void Start () {



	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void rotate(){
		Vector3 detailPos = transform.GetChild(0).position;
		Vector3 detailUp = transform.GetChild(0).up;

		Vector2 point = transform.position;
		point += new Vector2(0.1f*+transform.up.x,0.1f*+transform.up.y);

		_rot++;
		if(_rot>4){ 
			_rot = -4;
			transform.RotateAround(point, Vector3.forward, 180);
			
		}else{
			transform.RotateAround(point, Vector3.forward, 22.5f);
		}

		
		transform.GetChild(0).transform.position = detailPos;
		transform.GetChild(0).transform.up = detailUp;
	}

	public override void setColor(int color){

		if(color >= 5){
			if(color >=19) color = color - 14;
			transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = baseTextures[14];
			GetComponent<SpriteRenderer>().sprite = baseTextures[5];
		}else{			
			GetComponent<SpriteRenderer>().sprite = baseTextures[color];
		}

		this.color = color;
		GetComponentInChildren<Laser>().setColor(laserTextures[color], color);

	}
}
