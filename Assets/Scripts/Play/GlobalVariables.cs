﻿using UnityEngine;
using System.Collections;

public static class GlobalVariables {

	
	public const string resDirectory = "Assets/Resources/";
	public const string levelsDirectory = "levels/";

	public static int levelNum = 0;

	public static Texture2D[] laserTextures;

	public static int shelf= 0;
}
