﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class LevelSelect : MonoBehaviour {

	public GameObject levelButton;

	// Use this for initialization
	void Start () {
		createLevelList();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void createLevelList(){
		
		GameObject button;
		
		TextAsset[] levels = Resources.LoadAll<TextAsset>("Levels");

		int child = 0;

		int i = 0;
		foreach(TextAsset t in levels){
			button = (GameObject)Instantiate(levelButton);
			button.transform.SetParent(transform.GetChild(child));
			button.transform.localScale = new Vector3(1,1,1);
			
			button.GetComponentInChildren<Text>().text = t.name;
			button.name = t.name;

			button.GetComponent<btnLevelSelect>().pos = i;

			if(SaveData.data[i] == false) button.transform.GetChild(1).gameObject.SetActive(false);
			if(i<=SaveData.block +1 ) button.transform.GetChild(2).gameObject.SetActive(false);

			i++;

			if(i>=20*(child+1)) child++;
		}


	}


}
