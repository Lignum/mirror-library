﻿using UnityEngine;
using System.Collections;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class LevelCreator : MonoBehaviour {

	public GameObject wall;	
	public GameObject wallRamp;	
	public GameObject back;	
	public GameObject laserBase;
	public GameObject laserReceptor;
	public GameObject shelf;
	public GameObject glass;
	public GameObject wallMirror;

	public MirrorMng mirrorMng;

	private GameObject w;
	private GameObject o;
	private GameObject b;


	public GameObject playMenu;

	private static int receptors;
	private static int activeReceptors;

	private float contDown;
	private bool save;


	// Use this for initialization
	void Start () {
		Debug.Log("Play level: " + GlobalVariables.levelNum);

		createBaseObjects();
		loadlevel();

		contDown = 0.5f;

		save = false;
	}
	
	// Update is called once per frame
	void Update () {

		if(activeReceptors == receptors){
			contDown -= 1* Time.deltaTime;
			if(contDown<=0) levelComplete();
		}

	}

	private void createBaseObjects(){

		w = new GameObject();
		b = new GameObject();
		o = new GameObject();
		
		w.transform.parent = transform;
		b.transform.parent = transform;
		o.transform.parent = transform;

		w.name = "Walls";
		b.name = "Background";
		o.name = "Objects";
		
	}


	public void loadlevel(){

		Level level = null;
		
		TextAsset text = Resources.LoadAll<TextAsset>(GlobalVariables.levelsDirectory)[GlobalVariables.levelNum];
		Stream stream = new MemoryStream(text.bytes);
		
		BinaryFormatter bformatter = new BinaryFormatter();
		level = (Level)bformatter.Deserialize(stream);
		
		//Debug.Log(text);
		
		stream.Close();

		createLevel(level);

		mirrorMng.GetComponent<CameraMover>().setMaxPos(level.map.GetLength(0)-0.95f, level.map.GetLength(1)-1f);
	}

	private void createLevel(Level l){

		int i=0;
		int j=0;


		receptors = 0;
		activeReceptors = 0;
		//actualWidth = l.map.GetLength(0)-1;
		//actualHeight = l.map.GetLength(1)-1;
		//GetComponent<CameraMover>().setMaxPos(actualWidth, actualHeight);

		while(j<=l.map.GetLength(1)-1){
			
			loadLevelWalls(i,j,l);			
			loadLevelObjects(i,j,l);
			loadLevelBackground(i,j,l);
			
			if(i>=l.map.GetLength(0)-1){
				i=0;
				j++;
			}else{
				i++;
			}
			
		}

		loadMirrors(l);
		
	}
	
	private void loadLevelWalls(int i, int j, Level l){

		GameObject obj;

		GameObject wa = wall;

		if(l.map[i,j] != -1){

			if(l.map[i,j]>=60) wa = wallRamp;

			obj = (GameObject)Instantiate(wa, new Vector2(i,j), Quaternion.identity);
			obj.GetComponent<WallScr>().setKey(l.map[i,j]);
			obj.transform.parent = w.transform;
		}
	}
	
	private void loadLevelObjects(int i, int j, Level l){
		
		GameObject obj;

		if(l.objectsMap[i,j].color != -1){
				
			GameObject t = laserBase;
			//Debug.Log(l.objectsMap[i,j].type);
			
			switch(l.objectsMap[i,j].type){
			case 1: t = laserReceptor;
				receptors ++;
				break;
			case 2: t = shelf;
				break;
			case 3: t = wallMirror;
				break;
			case 4: t = glass;
				break;
			}
			
			obj = (GameObject)Instantiate(t, new Vector2(i,j), Quaternion.identity);
			obj.GetComponent<ObjectData>().setColor(l.objectsMap[i,j].color);
			
			obj.transform.rotation = l.objectsMap[i,j].rot();
			obj.transform.position = l.objectsMap[i,j].pos();
			obj.transform.GetChild(0).rotation = l.objectsMap[i,j].detailRot();
			obj.transform.parent = o.transform;

		}
	}

	private void loadLevelBackground(int i, int j, Level l){
		
		GameObject obj;

		if(l.backMap[i,j] != -1){
			obj = (GameObject)Instantiate(back, new Vector2(i,j), Quaternion.identity);
			obj.GetComponent<BackScr>().setKey(l.backMap[i,j]);
			obj.transform.parent = b.transform;
		}
	}

	private void loadMirrors(Level l){
		mirrorMng.setQuantity(new int[]{l.mirror, l.mirror45, l.mirror25, l.stone, l.transparent});
	}

	public static void activeReceptor(){
		
		//Debug.Log("act: " + activeReceptors + " - " + receptors);
		activeReceptors++;
	}

	public static void deactivateReceptor(){
		activeReceptors--;
	}

	private void levelComplete(){

		//Debug.Log("COMPLETE");

		if(!save){			
			SaveData.levelComplete(GlobalVariables.levelNum);
			save = true;
		}

		playMenu.GetComponent<PlayMenuScr>().options = false;
		playMenu.SetActive(true);

	}

}
