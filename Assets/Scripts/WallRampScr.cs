﻿using UnityEngine;
using System.Collections;

public class WallRampScr : WallScr {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void setKey(int key){
		
		base.setKey(key);

		switch(this.key){


		case 60:
		case 64:
			//none
			break;
		case 61:
		case 65:
			transform.GetChild(0).localScale = new Vector2(-1,1);
			break;
		case 62:
		case 70:
			transform.GetChild(0).localScale = new Vector2(1,-1);
			break;
		case 63:
		case 71:
			transform.GetChild(0).localScale = new Vector2(-1,-1);
			break;
		}



	}
}
