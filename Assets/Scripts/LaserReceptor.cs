﻿using UnityEngine;
using System.Collections;

public class LaserReceptor : ObjectData {

	public Texture2D receptorText;
	private Sprite[] receptorTextures;

	private static int _startSprite = 6;

	private GameObject _laser;

	private bool active;
	private AudioSource sound;
	private AudioSource deactiveSound;
	
	void Awake(){
		
		receptorTextures = Resources.LoadAll<Sprite>("Images/"+receptorText.name);
		

	}

	// Use this for initialization
	void Start () {
		transform.GetChild(0).transform.up = Vector2.up;

		active = false;
		sound = gameObject.GetComponent<AudioSource>();
		deactiveSound = gameObject.GetComponents<AudioSource>()[1];
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void activate(int color, GameObject laser){

		if(this.color == color){
			if(_laser == null) _laser = laser;
			transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.green;
	
			if(!active){
				sound.Play();
				active = true;

				LevelCreator.activeReceptor();
				
			}
		}
	}

	public void unActive(int color){
		
		if(this.color == color){
			transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.red;

			deactiveSound.Play();

			active = false;

			LevelCreator.deactivateReceptor();
		}
	}

	void OnCollisionEnter (Collision col){

	}

	public override void setColor(int color){
		this.color = color;
		GetComponent<SpriteRenderer>().sprite = receptorTextures[_startSprite+color];

	}
}
