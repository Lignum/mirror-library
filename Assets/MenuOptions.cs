﻿using UnityEngine;
using System.Collections;

public class MenuOptions : MonoBehaviour {

	public GameObject menu;

	// Use this for initialization
	void Start () {
		close();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void open(){
		menu.transform.parent.gameObject.SetActive(true);
		menu.SetActive(true);
	}
	public void close(){
		menu.transform.parent.gameObject.SetActive(false);
		menu.SetActive(false);
	}
}
